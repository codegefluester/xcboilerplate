//
//  AppDelegate.h
//  FBPlain
//
//  Created by Bjorn Kaiser on 7/31/15.
//  Copyright (c) 2015 Bjoern Kaiser. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

